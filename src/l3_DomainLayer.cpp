#include "hw/l3_DomainLayer.h"

bool ElectronicJournal::invariant() const {
  return !_name.empty() && !_surname.empty() && !_lastname.empty() && !_kaf.empty() &&
	  _name.size() <= MAX_NAME && _surname.size() <= MAX_SURNAME && _lastname.size() <= MAX_LASTNAME &&
	  _id >= MIN_ID && _year >= MIN_YEAR && _year <= MAX_YEAR && _rating >= MIN_RATING && _rating <= MAX_RATING &&
	  _kaf.size() <= MAX_KAF;
}

ElectronicJournal::ElectronicJournal(std::string name, std::string surname, std::string lastname, int id, int year, std::string kaf, AcademicDegree academicDegree, int rating):
_name(std::move(name)), _surname(std::move(surname)), _lastname(std::move(lastname)), _id(id), _year(year), _kaf(std::move(kaf)), _academicDegree(academicDegree), _rating(rating)
{
  assert(invariant());
}

std::string ElectronicJournal::getFIO() const {
  return _name + " " + _surname + " " + _lastname;
}

int ElectronicJournal::getId() const {
  return _id;
}

int ElectronicJournal::getYear() const {
  return _year;
}

int ElectronicJournal::getRating() const {
  return _rating;
}

const std::string& ElectronicJournal::getKaf() const {
  return _kaf;
}

std::string ElectronicJournal::getAcademicDegree() const {
  if (_academicDegree == AcademicDegree::assistant_professor) {
	return "Доцент";
  }
  if (_academicDegree == AcademicDegree::bachelor) {
	return "Бакалавр";
  }
  if (_academicDegree == AcademicDegree::graduate_student) {
	return "Аспирант";
  }
  if (_academicDegree == AcademicDegree::master) {
	return "Магистр";
  }
  if (_academicDegree == AcademicDegree::professor) {
	return "Профессор";
  }
  if (_academicDegree == AcademicDegree::senior_lecturer) {
	return "Старший преподаватель";
  }
  if (_academicDegree == AcademicDegree::teacher) {
	return "Преподаватель";
  }
  return "";
}


bool ElectronicJournal::write(std::ostream& os) {
writeString(os, _name);
writeString(os, _surname);
writeString(os, _lastname);
writeNumber(os, _id);
writeNumber(os, _year);
writeString(os, _kaf);
writeNumber(os, _academicDegree);
writeNumber(os, _rating);
return os.good();
}

std::shared_ptr<ICollectable> ItemCollector::read(std::istream &is) {
  std::string name = readString(is, MAX_NAME);
  std::string surname = readString(is, MAX_SURNAME);
  std::string lastname = readString(is, MAX_LASTNAME);
  int id = readNumber<int>(is);
  int year = readNumber<int>(is);
  std::string kaf = readString(is, MAX_KAF);
  auto academicDegree = readNumber<AcademicDegree>(is);
  int rating = readNumber<int>(is);
  return std::make_shared<ElectronicJournal>(name, surname, lastname, id, year, kaf, academicDegree, rating);
}



#ifndef LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
#define LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_
#include "hw/l4_InfrastructureLayer.h"

const int MAX_NAME = 50;
const int MAX_SURNAME = 50;
const int MAX_LASTNAME = 50;
const int MIN_ID = 1;
const int MIN_YEAR = 1900;
const int MAX_YEAR = 2300;
const int MIN_RATING = 0;
const int MAX_RATING = 100;
const int MAX_KAF = 10;

enum AcademicDegree {
  assistant_professor,
  professor,
  senior_lecturer,
  teacher,
  graduate_student,
  master,
  bachelor
};

class ElectronicJournal : public ICollectable {
 protected:
  bool invariant() const;
 public:
  ElectronicJournal() = delete;
  ElectronicJournal(const ElectronicJournal& p) = delete;
  ElectronicJournal& operator=(const ElectronicJournal& p) = delete;
  ElectronicJournal(std::string name, std::string surname, std::string lastname, int id, int year, std::string kaf, AcademicDegree academicDegree, int rating);
  std::string getFIO() const;
  int getId() const;
  int getYear() const;
  int getRating() const;
  const std::string& getKaf() const;
  std::string getAcademicDegree() const;
  bool write(std::ostream& os) override;
 private:
  std::string _name;
  std::string _surname;
  std::string _lastname;
  int _id;
  int _year;
  std::string _kaf;
  AcademicDegree _academicDegree;
  int _rating;
};

class ItemCollector: public ACollector {
 public:
  virtual std::shared_ptr<ICollectable> read(std::istream& is) override;
};

#endif //LAB3_INCLUDE_HW_L3_DOMAINLAYER_H_

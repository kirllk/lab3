#include "hw/l2_ApplicationLayer.h"

bool Application::performCommand(const std::vector<std::string> & args) {
  if (args.empty()) {
	return false;
  }

  if (args[0] == "l" || args[0] == "load") {
	std::string filename = (args.size() == 1) ? "hw.data" : args[1];
	if (!_col.loadCollection(filename)) {
	  _out.Output("Ошибка при загрузке файла '" + filename + "'");
	  return false;
	}
	return true;
  }

  if (args[0] == "s" || args[0] == "save") {
	std::string filename = (args.size() == 1) ? "hw.data" : args[1];
	if (!_col.saveCollection(filename)) {
	  _out.Output("Ошибка при сохранении файла '" + filename + "'");
	  return false;
	}
	return true;
  }

  if (args[0] == "c" || args[0] == "clean") {
	if (args.size() != 1) {
	  _out.Output("Некорректное количество аргументов команды clean");
	  return false;
	}
	_col.clean();
	return true;
  }

  if (args[0] == "a" || args[0] == "add") {
	if (args.size() != 9) {
	  _out.Output("Некорректное количество аргументов команды add");
	  return false;
	}

	_col.addItem(std::make_shared<ElectronicJournal>(args[1], args[2], args[3], std::stoi(args[4]), std::stoi(args[5]), args[6], getAcademicDegree(args[7]), std::stoi(args[8])));
	return true;
  }

  if (args[0] == "r" || args[0] == "remove") {
	if (args.size() != 2) {
	  _out.Output("Некорректное количество аргументов команды remove");
	  return false;
	}
	_col.removeItem(std::stoi(args[1]));
	return true;
  }

  if (args[0] == "u" || args[0] == "update") {
	if (args.size() != 10) {
	  _out.Output("Некорректное количество аргументов команды update");
	  return false;
	}

	_col.updateItem(stoul(args[1]), std::make_shared<ElectronicJournal>(args[2], args[3], args[4], std::stoi(args[5]), std::stoi(args[6]), args[7], getAcademicDegree(args[8]), std::stoi(args[9])));
	return true;
  }

  if (args[0] == "v" || args[0] == "view") {
	if (args.size() != 1) {
	  _out.Output("Некорректное количество аргументов команды view");
	  return false;
	}

	size_t count = 0;
	for(size_t i = 0; i < _col.getSize(); ++i) {
	  const ElectronicJournal & item = dynamic_cast<ElectronicJournal &>(*_col.getItem(i));
	  if (!_col.isRemoved(i)) {
		_out.Output("[" + std::to_string(i) + "]"
				  + " ФИО студента: " + item.getFIO()
				  + " ID студента: " + std::to_string(item.getId())
				  + " год поступления: " + std::to_string(item.getYear())
				  + " кафедра: " + item.getKaf()
				  + " учёная степень : " + item.getAcademicDegree()
				  + " рейтинг: " + std::to_string(item.getRating()));
		count ++;
	  }
	}

	_out.Output("Количество элементов в коллекции: " + std::to_string(count));
	return true;
  }

  _out.Output("Недопустимая команда '" + args[0] + "'");
  return false;
}


AcademicDegree Application::getAcademicDegree(const std::string& str) {
  if (str == "Доцент") {
	return AcademicDegree::assistant_professor;
  }
  if (str == "Бакалавр") {
	return AcademicDegree::bachelor;
  }
  if (str == "Аспирант") {
	return AcademicDegree::graduate_student;
  }
  if (str == "Магистр") {
	return AcademicDegree::master;
  }
  if (str == "Профессор") {
	return AcademicDegree::professor;
  }
  if (str == "Старший преподаватель") {
	return AcademicDegree::senior_lecturer;
  }
  if (str == "Преподаватель") {
	return AcademicDegree::teacher;
  }
  return AcademicDegree();
}

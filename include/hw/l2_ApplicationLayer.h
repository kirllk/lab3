#ifndef LAB3_INCLUDE_HW_L2_APPLICATIONLAYER_H_
#define LAB3_INCLUDE_HW_L2_APPLICATIONLAYER_H_
#include <string>
#include "hw/l3_DomainLayer.h"

class IOutput {
 public:
  virtual ~IOutput() = default;
  virtual void Output(std::string s) const = 0;
};

class Application {
 public:
  Application() = delete;
  Application(const Application &) = delete;
  Application & operator=(const Application &) = delete;
  explicit Application(const IOutput & out): _out(out) {}
  bool performCommand(const std::vector<std::string> & args);
 private:
  const IOutput& _out;
  ItemCollector _col;
  AcademicDegree getAcademicDegree(const std::string& str);
};

#endif //LAB3_INCLUDE_HW_L2_APPLICATIONLAYER_H_
